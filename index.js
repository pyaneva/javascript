// // Напишете програма за конвертиране на щатски долари (USD) в български лева (BGN). Закръглете резултата
// // до 2 цифри след десетичната запетая. Използвайте фиксиран курс между долар и лев: 1 USD = 1.79549 BGN.


// // function usd() {
// //     let usd = 20;
// //     let courseBGN = 1.79549;
// //     let bgn = usd * courseBGN;
// //     console.log(bgn.toFixed(2));
// // }

// // function usd() {
// //     let usd = 100;
// //     let courseBGN = 1.79549;
// //     let bgn = usd * courseBGN;
// //     console.log(bgn.toFixed(2));
// // }

// function usd() {
//     let usd = 12.5;
//     let courseBGN = 1.79549;
//     let bgn = usd * courseBGN;
//     console.log(bgn.toFixed(2));
// }

// // Напишете програма, която чете ъгъл в радиани (radians) и го преобразува в градуси (degrees). Използвайте
// // формулата: градус = радиан * 180 / π.Числото π в JavaScript програми е достъпно чрез Math.PI. Закръглете
// // резултата до най-близкото цяло число използвайки toFixed(0).
// //  вход: 3.1416, 6.2832, 0.7854, 0.5236

// // function RadianToDegree() {
// //     let radians = 3.1416;
// //     let degrees = radians * 180 / Math.PI;
// //     console.log(degrees.toFixed(0));
// // }

// // function RadianToDegree() {
// //     let radians = 6.2832;
// //     let degrees = radians * 180 / Math.PI;
// //     console.log(degrees.toFixed(0));
// // }

// // function RadianToDegree() {
// //     let radians = 0.7854;
// //     let degrees = radians * 180 / Math.PI;
// //     console.log(degrees.toFixed(0));
// // }


// function RadianToDegree() {
//     let radians = 0.5236;
//     let degrees = radians * 180 / Math.PI;
//     console.log(degrees.toFixed(0));
// }

// // Правоъгълник е зададен с координатите на два от
// // своите срещуположни ъгъла (x1, y1) – (x2, y2). Да се
// // пресметнат площта и периметъра му. Входът се
// // приема от конзолата. Числата x1, y1, x2 и y2 са
// // дадени по едно наред. Изходът се извежда на
// // конзолата и трябва да съдържа два реда с по една
// // число на всеки от тях – лицето и периметъра.
// // Закръглете резултата до 2 цифри след десетичната
// // запетая
// // Вход 60, 20, 10, 50 
// // Вход 30, 40, 70, -10
// // Вход 600.25, 500.75, 100.50, -200.5

// // function area() {
// //     let x1 = 60;
// //     let y1 = 20;
// //     let x2 = 10;
// //     let y2 = 50;
// //     let lenght = Math.abs(x1 - x2);
// //     let width = Math.abs(y1 - y2);
// //     let area = lenght * width;
// //     let perimeter = lenght * 2 + width * 2;
// //     console.log(perimeter.toFixed(2), area.toFixed(2));
// // }

// function area() {
//     let x1 = 30;
//     let y1 = 40;
//     let x2 = 70;
//     let y2 = -10;
//     let lenght = Math.abs(x1 - x2);
//     let width = Math.abs(y1 - y2);
//     let area = lenght * width;
//     let perimeter = lenght * 2 + width * 2;
//     console.log(perimeter.toFixed(2), area.toFixed(2));
// }

// // function area() {
// //     let x1 = 600.25;
// //     let y1 = 500.75;
// //     let x2 = 100.50;
// //     let y2 = -200.5;
// //     let lenght = Math.abs(x1 - x2);
// //     let width = Math.abs(y1 - y2);
// //     let area = lenght * width;
// //     let perimeter = lenght * 2 + width * 2;
// //     console.log(perimeter.toFixed(2), area.toFixed(2));
// // }


// // Шивашки цех приема поръчки за ушиване на покривки и карета за маси за заведения. Покривките са
// // правоъгълни, каретата са квадратни, броят им винаги е еднакъв. Покривката трябва да виси с 30 см от всеки
// // ръб на масата. Страната на каретата е половината от дължината на масите. Във всяка поръчка се включва
// // информация за броя и размерите на масите.
// // Напишете програма, която пресмята цената на поръчка в долари и в левове, като квадратен метър плат за
// // правоъгълна покривка струва 7 долара, а за каре – 9 долара. Курсът на долара е 1.85 лева.
// // Вход: 5-маси 1м-дължина 0.5-ширина

// function delivery() {
//     let tableCount = 5;
//     let tableLenght = 1;
//     let tableWidth = 0.5;
//     let coverUsd = 7;
//     let pladUsd = 9;
//     let coverArea = tableCount * (tableLenght + 2 * 0.30) * (tableWidth + 2 * 0.30);
//     let pladArea = tableCount * (tableLenght / 2) * (tableLenght / 2);
//     let totalInUsd = (coverArea * coverUsd) + (pladArea * pladArea);
//     console.log(totalInUsd.toFixed(2));
// }

// // Напишете програма, която получава цяло число и отпечатва ден от седмицата (на английски език), в граници [1...7] или отпечатва "Error" в случай, че въведеното число е невалидно. 

// function weeks() {
//     var week = 5
//     switch (week) {
//         case 0:
//             console.log('Monday')
//             break;
//         case 1:
//             console.log('Tuesday')
//             break
//         case 2:
//             console.log('Wednesday')
//             break
//         case 3:
//             console.log('Thursday')
//             break
//         case 4:
//             console.log('Friday')
//         case 5:
//         case 6:
//             console.log('Its Weekend')
//             break
//         default:
//             console.log('Error')
//     }
// }

// // Напишете програма, която отпечатва класа на животното според неговото име, въведено от потребителя.
// // 1.	dog -> mammal
// // 2.	crocodile, tortoise, snake -> reptile
// // 3.	others -> unknown

// function myFunction() {
//     let animals = document.getElementById('animal').value
//     let text

//     switch (animals) {
//         case 'dog':
//             text = 'I am mammal!'
//             break
//         case 'crocodile':
//         case 'tortoise':
//         case 'snake':
//             text = 'I am reptile'
//             break
//         default:
//             text = 'I have never heard that kind of an animal...'
//     }
//     document.getElementById("print").innerHTML = text
// }

// //   Да се напише конзолна програма, която прочита възраст (реално число) и пол ('m' или 'f'), въведени от потребителя, и отпечатва обръщение измежду следните:
// // •	"Mr." – мъж (пол 'm') на 16 или повече години
// // •	"Master" – момче (пол 'm') под 16 години
// // •	"Ms." – жена (пол 'f') на 16 или повече години
// // •	"Miss" – момиче (пол 'f') под 16 години

// function compiler() {
//     let gender = document.getElementById('genders').value
//     let age = document.getElementById('ages').value
//     let text

//     if (gender == 'f') {
//         if (age >= 16) {
//             text = 'Ms.'
//         }
//         else {
//             text = 'Miss'
//         }
//     }
//     else if (gender == 'm') {
//         if (age >= 16) {
//             text = 'Mr.'
//         }
//         else {
//             text = 'Master'
//         }
//     }
//     else {
//         text = 'No Gender Found 0.0!'
//     }


//     document.getElementById('show').innerHTML = text
// }

// // Предприемчив българин отваря квартални магазинчета в няколко града и продава на различни цени според града:
// // град / продукт	coffee	water	beer	sweets	peanuts
// // Sofia	0.50	0.80	1.20	1.45	1.60
// // Plovdiv	0.40	0.70	1.15	1.30	1.50
// // Varna	0.45	0.70	1.10	1.35	1.55
// // Напишете програма, която приема входен аргумент и изважда от него продукт (низ), град (низ) и количество (число), и пресмята и отпечатва колко струва съответното количество от избрания продукт в посочения град. 

// function loading() {
//     let product = document.getElementById('products').value
//     let town = document.getElementById('towns').value
//     let quantity = document.getElementById('quantities').value
//     let text
//     let totalSum
//     if (product === 'coffee') {
//         if (town === 'Sofia') {
//             totalSum = quantity * 0.50
//         } else if (town === 'Plovdiv') {
//             totalSum = quantity * 0.40
//         } else {
//             totalSum = quantity * 0.45
//         }
//     } else if (product === 'water') {
//         if (town === 'Sofia') {
//             totalSum = quantity * 0.80
//         } else if (town === 'Plovdiv') {
//             totalSum = quantity * 0.70
//         } else {
//             totalSum = quantity * 0.70
//         }
//     } else if (product === 'beer') {
//         if (town === 'Sofia') {
//             totalSum = quantity * 1.20
//         } else if (town === 'Plovdiv') {
//             totalSum = quantity * 1.15
//         } else {
//             totalSum = quantity * 1.10
//         }
//     } else if (product === 'sweets') {
//         if (town === 'Sofia') {
//             totalSum = quantity * 1.45
//         } else if (town === 'Plovdiv') {
//             totalSum = quantity * 1.30
//         } else {
//             totalSum = quantity * 1.35
//         }
//     } else if (product === 'peanuts') {
//         if (town === 'Sofia') {
//             totalSum = quantity * 1.60
//         } else if (town === 'Plovdiv') {
//             totalSum = quantity * 1.50
//         } else {
//             totalSum = quantity * 1.55
//         }
//     }
//     text = (totalSum)
//     document.getElementById('calculate').innerHTML = text
// }

// // 5.	Число в интервалa
// // Да се напише програма, която проверява дали въведеното от потребителя число е в интервала [-100, 100] и е различно от 0 и извежда "Yes", ако отговаря на условията, или "No" ако е извън тях.
// // Примерен вход и изход
// // вход	изход	
// // -25	Yes		0	

// function submit() {
//     let a = document.getElementById('writeNum').value
//     if (a >= -100 && a <= 100 && a !== 0) {
//         text = 'yes'
//     } else {
//         text = 'no'
//     }
//     document.getElementById('sub').innerHTML = text
// }

// // 6.	Плод или зеленчук?
// // Да се напише програма, която приема входен аргумент input  и от него изважда  име на продукт,  и проверява дали е плод или зеленчук.
// // •	Плодовете "fruit" имат следните възможни стойности:  banana, apple, kiwi, cherry, lemon и grapes
// // •	Зеленчуците "vegetable" имат следните възможни стойности:  tomato, cucumber, pepper и carrot
// // •	Всички останали са "unknown"
// // Да се изведе "fruit", "vegetable" или "unknown" според въведения продукт.
// // cases: banana, apple, kiwi, cherry, lemon и grapes
// // cases: : tomato, cucumber, pepper и carrot

// function find() {
//     let fNv = document.getElementById('fruitNveg').value
//     let text


//     switch (fNv) {
//         case 'banana':
//         case 'apple':
//         case 'kiwi':
//         case 'cherry':
//         case 'lemon':
//         case 'grapes':
//             text = 'Im fruit!'
//             break
//         case 'tomato':
//         case 'cucumber':
//         case 'peper':
//         case 'carrot':
//         case 'lemon':
//             text = 'Im vegetable!'
//             break
//         default:
//             text = 'I dont know!'
//     }
//     document.getElementById('kind').innerHTML = text
// }

// // Невалидно число
// // Дадено число е валидно, ако е в диапазона [100…200] или е 0. Да се напише програма, която приема аргумента input и изважда от него  цяло число, и печата "invalid" ако въведеното число не е валидно. 

// function findOut() {
//     let typeNum = document.getElementById('isItValid').value
//     let text
//     isValid = (typeNum == 0 || typeNum > 100 && typeNum < 200)
//     if (isValid) {
//         text = 'Valid Number'
//     } else {
//         text = 'Invalid Number'
//     }
//     document.getElementById('answer').innerHTML = text

// }

// // 8.	Магазин за плодове
// // Магазин за плодове през работните дни работи на следните цени:

// // плод	banana	apple	orange	grapefruit	kiwi	pineapple	grapes
// // цена	2.50	1.20	0.85	1.45	2.70	5.50	3.85
// // Събота и неделя магазинът работи на по-високи цени:
// // плод	banana	apple	orange	grapefruit	kiwi	pineapple	grapes
// // цена	2.70	1.25	0.90	1.60	3.00	5.60	4.20
// // Напишете програма, която приема като вход аргумента input и изважда от него  плод (banana / apple / orange / grapefruit / kiwi / pineapple / grapes), ден от седмицата (Monday / Tuesday / Wednesday / Thursday / Friday / Saturday / Sunday) и количество (число, и пресмята цената според цените от таблиците по-горе. Резултатът да се отпечата закръглен с 2 цифри след десетичната точка. При невалиден ден от седмицата или невалидно име на плод да се отпечата "error". 

// function fruitPrice() {
//     let fruits = document.getElementById('fruit').value
//     let days = document.getElementById('day').value
//     let numQuantities = document.getElementById('numQuantity').value
//     let totalMoney = 0
//     let text
//     if (days === 'Monday' || days === 'Tuesday' || days === 'Wednesday' || days === 'Thursday' || days === 'Friday') {
//         switch (fruits) {
//             case 'banana':
//                 totalMoney = numQuantities * 2.50
//                 break
//             case 'apple':
//                 totalMoney = numQuantities * 1.20
//                 break
//             case 'orange':
//                 totalMoney = numQuantities * 0.85
//                 break
//             case 'grapefruit':
//                 totalMoney = numQuantities * 1.45
//                 break
//             case 'kiwi':
//                 totalMoney = numQuantities * 2.70
//                 break
//             case 'pineapple':
//                 totalMoney = numQuantities * 5.50
//                 break
//             case 'grapes':
//                 totalMoney = numQuantities * 3.85
//                 break
//             default:
//                 text = 'error'
//         }
//     } else if (days === 'Saturday' || days === 'Sunday') {
//         switch (fruits) {
//             case 'banana':
//                 totalMoney = numQuantities * 2.70
//                 break
//             case 'apple':
//                 totalMoney = numQuantities * 1.25
//                 break
//             case 'orange':
//                 totalMoney = numQuantities * 0.90
//                 break
//             case 'grapefruit':
//                 totalMoney = numQuantities * 1.60
//             case 'kiwi':
//                 totalMoney = numQuantities * 3.00
//                 break
//             case 'pineapple':
//                 totalMoney = numQuantities * 5.60
//                 break
//             case 'grapes':
//                 totalMoney = numQuantities * 4.20
//                 break
//             default:
//                 text = 'error'
//         }
//     }
//     text = (totalMoney.toFixed(2))
//     document.getElementById('showPrice').innerHTML = text
// }

// // 	Търговски комисионни
// // Фирма дава следните комисионни на търговците си според града, в който работят и обема на продажбите:
// // Град	0 ≤ s ≤ 500	500 < s ≤ 1 000	1 000 < s ≤ 10 000	s > 10 000
// // Sofia	5%	  7%	 8%	  12%
// // Varna	4.5%  7.5%  10%	  13%
// // Plovdiv	5.5%  8%	12%	  14.5%
// // Напишете конзолна програма, която приема входен аргумент input и извадете от него име на град (стринг) и обем на продажби (число) и изчислява и извежда размера на търговската комисионна според горната таблица. Резултатът да се изведе форматиран до 2 цифри след десетичната точка. При невалиден град или обем на продажбите (отрицателно число) да се отпечата "error". 

// function commission() {
//     let cities = document.getElementById('city').value
//     let sales = document.getElementById('sale').value
//     let text
//     let Sum = 0
//     if (sales >= 0 && sales <= 500) {
//         switch (cities) {
//             case 'Sofia':
//                 Sum = (parseFloat(sales) + (5 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Plovdiv':
//                 Sum = (parseFloat(sales) + (4.5 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Varna':
//                 Sum = (parseFloat(sales) + (5.5 / 100 * sales) - parseFloat(sales))
//                 break
//         }
//     }
//     else if (sales >= 500 && sales <= 1000) {
//         switch (cities) {
//             case 'Sofia':
//                 Sum = (parseFloat(sales) + (7 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Plovdiv':
//                 Sum = (parseFloat(sales) + (7.5 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Varna':
//                 Sum = (parseFloat(sales) + (8 / 100 * sales) - parseFloat(sales))
//                 break
//         }
//     }
//     else if (sales >= 1000 && sales <= 10000) {
//         switch (cities) {
//             case 'Sofia':
//                 Sum = (parseFloat(sales) + (8 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Plovdiv':
//                 Sum = (parseFloat(sales) + (10 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Varna':
//                 Sum = (parseFloat(sales) + (12 / 100 * sales) - parseFloat(sales))
//                 break
//         }
//     }
//     else if (sales > 10000) {
//         switch (cities) {
//             case 'Sofia':
//                 Sum = (parseFloat(sales) + (12 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Plovdiv':
//                 Sum = (parseFloat(sales) + (13 / 100 * sales) - parseFloat(sales))
//                 break
//             case 'Varna':
//                 Sum = (parseFloat(sales) + (14.5 / 100 * sales) - parseFloat(sales))
//                 break
//         }
//     } else {
//         alert('error')
//         Sum = ""
//     }
//     text = (Sum.toFixed(2))
//     document.getElementById('showCommission').innerHTML = text
// }

// // 10.	*Ски почивка
// // Атанас решава да прекара отпуската си в Банско и да кара ски. Преди да отиде обаче, трябва да резервира хотел и да изчисли колко ще му струва престоя. Съществуват следните видове помещения, със следните цени за престой:
// // 	"room for one person" – 18.00 лв за нощувка
// // 	"apartment" – 25.00 лв за нощувка 
// // 	"president apartment" – 35.00 лв за нощувка
// // Според броят на дните, в които ще остане в хотела (пример: 11 дни = 10 нощувки) и видът на помещението, което ще избере, той може да ползва различно намаление. Намаленията са както следва:
// // вид помещение	по-малко от 10 дни	между 10 и 15 дни	повече от 15 дни
// // room for one person	не ползва намаление	не ползва намаление	не ползва намаление
// // apartment	30% от крайната цена	35% от крайната цена	50% от крайната цена
// // president apartment	10% от крайната цена	15% от крайната цена	20% от крайната цена
// // След престоя, оценката на Атанас за услугите на хотела може да е позитивна (positive) или негативна (negative) . Ако оценката му е позитивна, към цената с вече приспаднатото намаление Атанас добавя 25% от нея. Ако оценката му е негативна приспада от цената 10%.

// function priceRoom() {
//     let daysHotel = document.getElementById('dayHotel').value
//     let roomType = document.getElementById('room').value
//     let feedback = document.getElementById('grade').value
//     let nightCount = daysHotel - 1
//     let priceHotel = 0

//     switch (roomType) {
//         case 'room for one person':
//             priceHotel = 18 * nightCount
//             break
//         case 'apartment':
//             priceHotel = 25
//             if (nightCount < 10) {
//                 priceHotel = priceHotel * nightCount * 0.7
//             }
//             else if (nightCount > 10 && nightCount < 15) {
//                 priceHotel = priceHotel * nightCount * 0.65
//             }
//             else if (nightCount > 15) {
//                 priceHotel = priceHotel * nightCount * 0.5
//             }
//             break
//         case 'president apartment':
//             priceHotel = 35
//             if (nightCount < 10) {
//                 priceHotel = priceHotel * nightCount * 0.9
//             }
//             else if (nightCount > 10 && nightCount < 15) {
//                 priceHotel = priceHotel * nightCount * 0.85
//             }
//             else if (nightCount > 15) {
//                 priceHotel = priceHotel * nightCount * 0.8
//             }
//             break
//     }
//     switch (feedback) {
//         case 'positive':
//             priceHotel = priceHotel + (0.25 * priceHotel)
//             break
//         case 'negative':
//             priceHotel = priceHotel - (0.1 * priceHotel)
//             break
//     }

//     text = (priceHotel.toFixed(2))
//     document.getElementById('output').innerHTML = text
// }

// function solve() {
//     for (let i = 1; i <= 100; i = i + 1) {
//         console.log(i)
//     }
// }
// solve();


// // Shopping List 
// var sum = 0
// function addItem() {
//     var itemInput = document.getElementById("name")
//     var priceInput = document.getElementById("price")
//     var list = document.getElementById("list")

//     var currentItem = itemInput.value + ' - ' + priceInput.value
//     var listItem = document.createElement("li")
//     listItem.innerHTML = currentItem
//     list.appendChild(listItem)
//     itemInput.value = ""
//     sum = parseFloat(sum) + parseFloat(priceInput.value)
//     text = sum
//     document.getElementById('totalPrice').innerHTML = text

//     var span = document.createElement("SPAN");
//     var txt = document.createTextNode("\u00D7");
//     span.className = "close";
//     span.appendChild(txt);
//     listItem.appendChild(span);

//     span.onclick = function () {
//         var div = this.parentElement;
//         div.style.display = "none";
//     }
// }


// function checkLength(event) {
//     event.preventDefault()
//     let textbox = document.getElementById("textbox");
//     let text
//     let password = document.getElementById("txtPassword").value
//     let confirmPassword = document.getElementById("txtConfirmPassword").value;
//     if (textbox.value.length >= 10) {
//         text = "The name must be less than 100 symbols"
//         document.getElementById('nameError').innerHTML = text
//     }
//     if (password != confirmPassword) {
//         text = "Passwords do not match."
//         document.getElementById('passError').innerHTML = text
//     }
//     if (!terms.checked) {
//         text = "You <strong>must</strong> agree to the terms first."
//         document.getElementById('checkboxError').innerHTML = text
//     }
//     if (password.length <= 5) {
//         text = "The password <strong> must</strong> be more that 5 symbols"
//         document.getElementById('passError').innerHTML = text
//     }

//     console.log(checkUppercase(password))
//     console.log(checkPass(password))
//     if (!checkUppercase(password)) {
//         text = "The password <strong> must</strong> contain at least one uppercase character"
//         document.getElementById('passError').innerHTML = text
//     }
//     if (!checkPass(password)) {
//         text = "The password <strong> must</strong> contain at least one digit"
//         document.getElementById('passError').innerHTML = text
//     }
// }
// function checkUppercase(text) {
//     for (let i = 0; i < text.length; i++) {
//         if (text.charCodeAt(i) >= 65 && text.charCodeAt(i) <= 90) {
//             return true
//         }
//     }
//     return false
// }

// function checkPass(numbers) {
//     for (let i = 0; i < numbers.length; i++) {
//         if (numbers.charCodeAt(i) >= 48 && numbers.charCodeAt(i) <= 57) {
//             return true
//         }
//     }
//     return false

// }

// Registration Form
// $(function () {

//    $('#fname_error_message').hide()
//    $('#sname_error_message').hide()
//    $('#email_error_message').hide()
//    $('#password_error_message').hide()
//    $('retype_password_error_message').hide()

//    let error_fname = false
//    let error_sname = false
//    let error_email = false
//    let error_password = false
//    let error_retype_password = false


//    $("#form_fname").focusout(function () {
//       check_fname();
//    });
//    $("#form_sname").focusout(function () {
//       check_sname();
//    });
//    $("#form_email").focusout(function () {
//       check_email();
//    });
//    $("#form_password").focusout(function () {
//       check_password();
//    });
//    $("#form_retype_password").focusout(function () {
//       check_retype_password();
//    });



//    function check_fname() {
//       let pattern = /^[a-zA-Z]*$/;
//       let fname = $("#form_fname").val();
//       if (pattern.test(fname) && fname !== '') {
//          $("#fname_error_message").hide();
//          $("#form_fname").css("border-bottom", "2px solid #34F458");
//       } else {
//          $("#fname_error_message").html("Should contain only Characters");
//          $("#fname_error_message").show();
//          $("#form_fname").css("border-bottom", "2px solid #F90A0A");
//          error_fname = true;
//       }
//    }

//    function check_sname() {
//       let pattern = /^[a-zA-Z]*$/;
//       let sname = $("#form_sname").val()
//       if (pattern.test(sname) && sname !== '') {
//          $("#sname_error_message").hide();
//          $("#form_sname").css("border-bottom", "2px solid #34F458");
//       } else {
//          $("#sname_error_message").html("Should contain only Characters");
//          $("#sname_error_message").show();
//          $("#form_sname").css("border-bottom", "2px solid #F90A0A");
//          error_fname = true;
//       }
//    }

//    function check_password() {
//       let password_length = $("#form_password").val().length;
//       if (password_length < 8) {
//          $("#password_error_message").html("Atleast 8 Characters");
//          $("#password_error_message").show();
//          $("#form_password").css("border-bottom", "2px solid #F90A0A");
//          error_password = true;
//       } else {
//          $("#password_error_message").hide();
//          $("#form_password").css("border-bottom", "2px solid #34F458");
//       }
//    }

//    function check_retype_password() {
//       let password = $("#form_password").val();
//       let retype_password = $("#form_retype_password").val();
//       if (password !== retype_password) {
//          $("#retype_password_error_message").html("Passwords Did not Matched");
//          $("#retype_password_error_message").show();
//          $("#form_retype_password").css("border-bottom", "2px solid #F90A0A");
//          error_retype_password = true;
//       } else {
//          $("#retype_password_error_message").hide();
//          $("#form_retype_password").css("border-bottom", "2px solid #34F458");
//       }
//    }
//    function check_email() {
//       let pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//       let email = $("#form_email").val();
//       if (pattern.test(email) && email !== '') {
//          $("#email_error_message").hide();
//          $("#form_email").css("border-bottom", "2px solid #34F458");
//       } else {
//          $("#email_error_message").html("Invalid Email");
//          $("#email_error_message").show();
//          $("#form_email").css("border-bottom", "2px solid #F90A0A");
//          error_email = true;
//       }
//    }

//    $('#nextBtn').click(function () {
//       $('#next_page').show();
//       $('#registration_form').hide();
//    });

//    $("#backBtn").click(function () {
//       $('#registration_form').show();
//       $('#next_page').hide();
//    });
//    $("#nextBtn").click(function () {
//       error_fname = false;
//       error_sname = false;
//       error_email = false;
//       error_password = false;
//       error_retype_password = false;

//       check_fname();
//       check_sname();
//       check_email();
//       check_password();
//       check_retype_password();

//       if (error_fname === false && error_sname === false && error_email === false && error_password === false && error_retype_password === false) {
//          return true;
//       } else {
//          $('#registration_form').show();
//          $('#next_page').hide();
//          return false;
//       }

//    });
//    $("#register").click(function () {
//       error_fname = false;
//       error_sname = false;
//       error_email = false;
//       error_password = false;
//       error_retype_password = false;

//       check_fname();
//       check_sname();
//       check_email();
//       check_password();
//       check_retype_password();

//       if (error_fname === false && error_sname === false && error_email === false && error_password === false && error_retype_password === false) {
//          alert("Registration Successfull");
//          return true;
//       } else {
//          $('#next_page').hide();
//          return false;
//       }

//    });
// });


// Shopping List
// function removeLi() {
//    $(this).parent().remove();
// }

// $(document).ready(function () {
//    $('.addButton').click(function () {
//       let text = $('#list-item').val();
//       $('.add').append('<li><input type=checkbox>' + text + '&nbsp;&nbsp;<span class="delete" style="cursor: pointer"><i class="fas fa-times"></i></span>' + '</li>');
//       $('#list-item').val('').focus();
//       $('#list-number').val('').focus();
//       $('#total').val().replace('')
//    });


//    $('body').on('click','.delete',function(){
//       $(this).parent().remove();
//    })
//    $("#move").click(function() {
//    if ($(':checkbox').is(':checked')){
//       return $('li :checked').parent().appendTo('.checkedItems');
//    }
//    });
// });


// Buttons
// $(".red").click(function () {
//    $(this).parent().find('p').css('background-color', 'red')
// })
// $(".green").click(function () {
//    $(this).parent().find('p').css('background-color', 'green')
// })
// $(".blue").click(function () {
//    $(this).parent().find('p').css('background-color', 'blue')
// })


$(document).ready(function () {

   let move = 1;
   let play = true;

   $("#board tr td").click(function () {
      if ($(this).text() == "" && play) {
         if ((move % 2) == 1) {
            $(this).append("X");
            $(this).css('color', "#61892f");

         } else {
            $(this).append("O");
            $(this).css('color', "#e85a4f");
         }
         move++;
         if (checkForWinner() != -1 && checkForWinner() != "") {
            if (checkForWinner() == "X") {
               $('body').append('<div class="winner"><span>Winner</span>X</div><button onclick="location.reload();" id="reload">Play Again</button>');
               $('.winner').css('background-color', '#61892f');
               $('#reload').css('color', '#61892f');
            } else {
               $('body').append('<div class="winner"><span>Winner</span>O</div><button onclick="location.reload();" id="reload">Play Again</button>');
               $('.winner').css('background-color', '#e85a4f');
               $('#reload').css('color', '#e85a4f');
            }
            play = false;
         }
      }
   });

   function checkForWinner() {
      let space1 = $("#board tr:nth-child(1) td:nth-child(1)").text();
      let space2 = $("#board tr:nth-child(1) td:nth-child(2)").text();
      let space3 = $("#board tr:nth-child(1) td:nth-child(3)").text();
      let space4 = $("#board tr:nth-child(2) td:nth-child(1)").text();
      let space5 = $("#board tr:nth-child(2) td:nth-child(2)").text();
      let space6 = $("#board tr:nth-child(2) td:nth-child(3)").text();
      let space7 = $("#board tr:nth-child(3) td:nth-child(1)").text();
      let space8 = $("#board tr:nth-child(3) td:nth-child(2)").text();
      let space9 = $("#board tr:nth-child(3) td:nth-child(3)").text();
      // check rows
      if ((space1 == space2) && (space2 == space3)) {
         return space3;
      } else if ((space4 == space5) && (space5 == space6)) {
         return space6;
      } else if ((space7 == space8) && (space8 == space9)) {
         return space9;
      }
      // check columns
      else if ((space1 == space4) && (space4 == space7)) {
         return space7;
      } else if ((space2 == space5) && (space5 == space8)) {
         return space8;
      } else if ((space3 == space6) && (space6 == space9)) {
         return space9;
      }
      // check diagonals
      else if ((space1 == space5) && (space5 == space9)) {
         return space9;
      } else if ((space3 == space5) && (space5 == space7)) {
         return space7;
      }
      // no winner
      return -1;
   }

});


// Loop Digit Task - 1
// function number() {
//    $("#find").click(function () {
//       let number = parseInt($("#numbers").val())

//       for (let i = 1; i <= 999; i++) {
//          if (i % 10 === number) {
//             console.log(i)
//          }
//       }
//    }
//    )
// }
// number()

// SU Loop Task - 2
function solution(input) {
   let numbers = Number(input.shift())
   
   let maxNumber = Number.MIN_SAFE_INTEGER
   let sum = 0

   for (let number = 1; number <= numbers; number++) {
      let currentNumber = Number(input.shift())
      sum = sum + currentNumber 
      if (currentNumber > maxNumber) {
         maxNumber = currentNumber 
      }
   }
   let result = sum - maxNumber

   if(result === maxNumber) {
      console.log(`Yes`)
      console.log(`Sum = ${result}`)
   }
}

solution ([
   '7',
   '3',
   '4',
   '1',
   '1',
   '1',
   '2',
   '12',
   '1'
])

// SU Loop - 3 Task
// function solution(input) {
//    let numbers = Number(input.shift())
//    let oddSum = 0
//    let oddMin = Number.MAX_SAFE_INTEGER
//    let oddMax = Number.MIN_SAFE_INTEGER
//    let evenSum = 0
//    let evenMin = Number.MAX_SAFE_INTEGER
//    let evenMax = Number.MIN_SAFE_INTEGER
//    for (let position = 1; position <= numbers; position++) {
//       let currentNumber = Number(input.shift())
//       if (position % 2 === 1) {
//          oddSum += currentNumber
//          if(currentNumber < oddMin) {
//             oddMin = currentNumber
//          }
//          if (currentNumber > oddMax) {
//             oddMax = currentNumber
//          }
//       } else {
//          evenSum += currentNumber 
//          if (currentNumber < evenMin) {
//             evenMin = currentNumber
//          }
//          if (currentNumber > evenMax ) {
//             evenMax = currentNumber
//          }
//       }
//    }
//    console.log(`OddSum=${oddSum},`)
//    console.log(`OddMin=${oddMin},`)
//    console.log(`OddMax=${oddMax}`)
//    console.log(`EvenSum=${evenSum},`)
//    console.log(`EvenMin=${evenMin},`)
//    console.log(`EvenMax=${evenMax}`)
// }

// solution([
//    '4',
//    '1.5',
//    '1.75',
//    '1.5',
//    '1.75'
// ])

// SU Task - 4 Loop

// function solution(input) {
//    let numbers = Number(input.shift())

//    let p1 = 0 // < 200
//    let p2 = 0 // 200 - 399
//    let p3 = 0 // 400 - 599
//    let p4 = 0 // 600 - 799
//    let p5 = 0 // 800 >=

//    for(let n = 1; n<= numbers; n++) {
//       let currentNumber = Number(input.shift())

//       if (currentNumber < 200) {
//          p1++
//       } else if (currentNumber >= 200 && currentNumber <= 399) {
//          p2++
//       } else if (currentNumber >= 400 && currentNumber <= 599) {
//          p3++
//       } else if (currentNumber >= 600 && currentNumber <= 799) {
//          p4++
//       } else {
//          p5++
//       }
//    }
//    console.log(p1,p2,p3,p4,p5)

//    let p1Percent = p1 / numbers * 100 
//    let p2Percent = p2 / numbers * 100 
//    let p3Percent = p3 / numbers * 100 
//    let p4Percent = p4 / numbers * 100 
//    let p5Percent = p5 / numbers * 100 

//    console.log(`${p1Percent.toFixed(2)}%`)
//    console.log(`${p2Percent.toFixed(2)}%`)
//    console.log(`${p3Percent.toFixed(2)}%`)
//    console.log(`${p4Percent.toFixed(2)}%`)
//    console.log(`${p5Percent.toFixed(2)}%`)
// }

// solution([
//    '7',
//    '800',
//    '801',
//    '250',
//    '199',
//    '399',
//    '599',
//    '799'
// ])


// SU Task - 5 Loop

// function solution(input) {
//    let numbers = Number(input.shift())
//    p1 = 0
//    p2 = 0
//    p3 = 0
//    for(let n = 1; n<= numbers; n++) {
//       let currentNumber = Number(input.shift())
//       if (currentNumber % 2 === 0) {
//          p1++
//       }
//       if (currentNumber % 3 === 0) {
//          p2++
//       }
//       if (currentNumber % 4 === 0) {
//          p3++
//       }
//    }
//    console.log(p1)

//    let p1Percent = p1 / numbers * 100 
//    let p2Percent = p2 / numbers * 100 
//    let p3Percent = p3 / numbers * 100 

//    console.log(`${p1Percent.toFixed(2)}%`)
//    console.log(`${p2Percent.toFixed(2)}%`)
//    console.log(`${p3Percent.toFixed(2)}%`)
// }
//    solution([
//       '10',
//       '680',
//       '2',
//       '600',
//       '200',
//       '800',
//       '799',
//       '199',
//       '46',
//       '128',
//       '65'
//    ])

//  SU Task - 6 Loop

// function solution(input) {
//    let openedTabs = Number(input.shift())
//    let salary = Number(input.shift())

//       for (let i = 1; i<= openedTabs; i++) {
//       let currentSite = input.shift()

//       if (salary <= 0) {
//          console.log(`You have lost your salary.`)
//          break
//       }

//          switch (currentSite) {
//             case 'Facebook':
//                salary -= 150
//                break
//          }
//          switch (currentSite) {
//             case 'Instagram':
//                salary -= 100
//                break
//          }
//          switch (currentSite) {
//             case 'Facebook':
//                salary -= 50
//                break
//          }
         
//          if(salary > 0) {
//             console.log(salary)
//          }

//       }
//    }
   
// solution([
//    '10',
//    '750',
//    'Facebook',
//    'Dev.bg',
//    'Instagram',
//    'Facebook',
//    'Reddit',
//    'Facebook',
//    'Facebook'
// ])

// ])// ]) 
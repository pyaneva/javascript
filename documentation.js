// String, Numbers, Boolean, null, undefined

const name = 'John' // string
const age = 30 // number
const rating = 4.5  // number
const isCool = true // booean
const x = null // object but its null
const y = undefined // undefined
let z; // undefined

console.log(typeof isCool)

// Concatenation 
console.log('My name is ' + name + ' and I am ' + age)

// Template String 
console.log(`My name is ${name} and I am ${age}`)

//  OR 

const hello = `My name is ${name} and I am ${age}`
console.log(hello)

// Arrays - variables that hold multiple values

// Array print all
const s = 'technlogy, computers, it, code'
console.log(s.split(', '))

// Array Numbers
const numbers = new Array(1,2,3,4,5);
console.log(numbers)

const fruit = ['apple', 'orange', 'pear']
console.log(fruit)

const fruits = ['apples', 'oranges', 'pears', 10, true]
console.log(fruits)